$(document).ready(function(){
var convertCur = $('#convertibleCurrency');
var neededCur = $("#neededCurrency");

	$(function() {
		$.getJSON('http://university.netology.ru/api/currency', function(data){
			$.each(data, function(key, val){

				function currencyData(name) {
					if ((!name.data('value')) || (!name.data('nominal'))) {
						name.data('value', val.Value);
						name.data('nominal', val.Nominal);
						crtOption(name);
					} else {
						crtOption(name);
					}
				}
				function crtOption(name){
					$('<option />').text(val.Name).attr({value :val.Value, data: val.Nominal}).appendTo(name);	
				}
				currencyData($('.currency1'));
				currencyData($('.currency2'));		  		
     		});
		});	
	});

	$('.currency1').on('change', function(e){
		$('.currency1').data('nominal', $('option:selected')[0].attributes[1].value).data('value', e.target.value);
		converter('.currency1', '.currency2', convertCur, neededCur);
	});

	$('.currency2').on('change', function(e){
		$('.currency2').data('nominal', $('option:selected')[0].attributes[1].value).data('value', e.target.value);
		converter('.currency2', '.currency1', neededCur, convertCur);
	});


	function converter(currency1, currency2, convert, needed) {

		if (($(currency1).data('value') === $(currency2).data('value')) || (!convert.val())) {

			needed.val(convert.val());

		} else {	
			needed.val((convert.val() * (($(currency1).data('value') / $(currency1).data('nominal'))/ 
			($(currency2).data('value') / $(currency2).data('nominal')))).toFixed(2));
		}
	}

		neededCur.val('');
		convertCur.val('');

		convertCur.keyup(function(){
			if ((!isNaN(parseFloat(convertCur.val()) && isFinite(convertCur.val()))) || 
				(!isNaN(parseFloat(neededCur.val()) && isFinite(neededCur.val())))){
				
				converter('.currency1', '.currency2', convertCur, neededCur);

				$('.wrapper p').addClass('text');

			} else {
				$('.wrapper p').text("Введите число!").removeClass('text');
				}
			if (!convertCur.val().length) {
				$('.wrapper p').addClass('text')
				neededCur.val('');
			}
			if (isNaN(neededCur.val())) {
				neededCur.val('');
			}
		});
		neededCur.keyup(function(){
			if ((!isNaN(parseFloat(convertCur.val()) && isFinite(convertCur.val()))) || 
				(!isNaN(parseFloat(neededCur.val()) && isFinite(neededCur.val())))){

				converter('.currency2', '.currency1', neededCur, convertCur);

				$('.wrapper p').addClass('text');

			} else {
				$('.wrapper p').text("Введите число!").removeClass('text');
				}
			if (!convertCur.val().length) {
				$('.wrapper p').addClass('text')
				convertCur.val('');
			}
			if (isNaN(convertCur.val())) {
				convertCur.val('');
			}
		});
});